#pragma once
#include "MyString.h"
template<typename T>
Utils::MyString<T>::MyString()
{
	m_length = 0;
	m_size = 0;
	m_str = NULL;
}
template<typename T>
Utils::MyString<T>::MyString(const T* otherString)
{
	m_length = 0;
	m_size = 0;
	m_str = NULL;
	Assign(otherString);
}
template<typename T>
Utils::MyString<T>::MyString(MyString<T>& otherString) :
	m_length(otherString.m_length),
	m_size(otherString.m_size)
{
	m_str = new T[ m_size ];
	memcpy_s(m_str, m_size * sizeof(T), otherString.m_str, otherString.m_size*sizeof(T));
}
template<typename T>
Utils::MyString<T>::~MyString()
{
	Clear();
}
template<typename T>
size_t Utils::MyString<T>::GetLength() const
{
	return m_length;
}
template<typename T>
T* Utils::MyString<T>::getStr() const
{
	return m_str;
}
template<typename T>
void Utils::MyString<T>::Assign(const Utils::MyString<T>& otherString)
{
	size_t len = otherString.m_length;
	if(len >= m_size)
	{
		Clear();
		m_str = new T[ len + 1 ];
		m_size = len + 1;
	}
	memcpy_s(m_str, len * sizeof(T), otherString.m_str, len * sizeof(T));
	m_str[ len ] = NULL;
	m_length = len;
}
template<typename T>
void Utils::MyString<T>::Assign(const T* otherString)
{
	size_t len = Strlen<T>()(otherString);
	if(len >= m_size)
	{
		Clear();
		m_str = new T[ len + 1 ];
		m_size = len + 1;
	}
	memcpy_s(m_str, len * sizeof(T), otherString, len * sizeof(T));
	m_str[ len ] = NULL;
	m_length = len;
}
template<typename T>
T Utils::MyString<T>::CharAt(const size_t index) const
{
	if(index < m_length)
	{
		return m_str[ index ];
	}
	std::cout << "unvalid index";
	return NULL;
}
template<typename T>
void Utils::MyString<T>::Append(const Utils::MyString<T>& otherString)
{
	size_t len = m_length + otherString.m_length;
	if(otherString.m_length >= m_size - m_length)
	{
		size_t length = this->m_length;
		T* tmp = new T[ length + 1 ];
		memcpy_s(tmp, (length+1) * sizeof(T), m_str, length * sizeof(T));
		Clear();
		m_str = new T[ len + 1 ];
		m_size = len + 1;
		memcpy_s(m_str, length * sizeof(T), tmp, length * sizeof(T));
		m_str[ length ] = NULL;
	}
	Utils::Strncat_s<T>()(m_str, m_size, otherString.m_str, otherString.m_length);
	m_str[ len ] = NULL;
	m_length = len;
}
template<typename T>
void Utils::MyString<T>::Append(const T* otherString)
{
	size_t otherLen = Strlen<T>()(otherString);
	size_t len = m_length + otherLen;
	if(otherLen >= m_size - m_length)
	{
		size_t length = this->m_length;
		T* tmp = new T[ length + 1 ];
		memcpy(tmp, m_str, length * sizeof(T));
		Clear();
		m_str = new T[ len + 1 ];
		m_size = len + 1;
		memcpy_s(m_str, length * sizeof(T), tmp, length * sizeof(T));
		m_str[ length ] = NULL;
	}
	Utils::Strncat_s<T>()(m_str, m_size, otherString, otherLen);
	m_str[ len ] = NULL;
	m_length = len;
}
template<typename T>
int Utils::MyString<T>::Compare(const Utils::MyString<T>& otherString) const
{
	return Utils::Strcmp<T>()(m_str, otherString.m_str);
}
template<typename T>
int Utils::MyString<T>::Compare(const T* otherString) const
{
	return Utils::Strcmp<T>()(m_str, otherString);
}
template<typename T>
bool Utils::MyString<T>::IsEmpty() const
{
	return m_length == 0;
}
template<typename T>
void Utils::MyString<T>::Clear()
{
	if(m_str)
	{
		delete m_str;
		m_str = NULL;
	}
	m_length = 0;
	m_size = 0;
}
template<typename T>
bool Utils::MyString<T>::operator==(const MyString<T>& a)
{
	if(a.m_length == this->m_length)
	{
		return Utils::Strcmp<T>()(this->m_str, a.m_str) == 0;
	}
	return false;
}
template<typename T>
Utils::MyString<T>& Utils::MyString<T>::operator=(const MyString<T>& a)
{
	this->Assign(a);
	return *this;
}
