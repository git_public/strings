#pragma once
#include "MyString.h"
#include "MyString.cpp"

using namespace Utils;

int main()
{
	char* str1 = "this is my first string";
	char* str2 = "this is my second string";
	Utils::MyString<char> s1(str1), s2(str2);
	
	std::cout << s1.IsEmpty() << std::endl;
	std::cout << s1.GetLength() << std::endl;
	std::cout << s1.Compare(s2) << std::endl;
	std::cout << s1.CharAt(10) << std::endl;
	s1.Append(s2);
	std::cout << "" << s1 << std::endl;
	s1.Append(str1);
	std::cout << s1 << std::endl;
	s1=s2;
	std::cout << s1 << std::endl;
	s1.Assign(str1);
	std::cout << s1 << std::endl;
	Utils::MyString<char> s3(s1);
	std::cout << s3 << std::endl;
	std::cout << (s1 == s1) << std::endl;

	wchar_t* str3 = L"this is my thired string";
	wchar_t* str4 = L"this is my forth string";
	Utils::MyString<wchar_t> s4(str3), s5(str4);

	std::wcout << s4.IsEmpty() << std::endl;
	std::wcout << s4.GetLength() << std::endl;
	std::wcout << s4.Compare(s5) << std::endl;
	std::wcout << s4.CharAt(10) << std::endl;
	s4.Append(s5);
	std::wcout << "" << s4 << std::endl;
	s4.Append(str3);
	std::wcout << s4 << std::endl;
	s4 = s5;
	std::wcout << s4 << std::endl;
	s4.Assign(str3);
	std::wcout << s4 << std::endl;
	Utils::MyString<wchar_t> s6(s4);
	std::wcout << s6 << std::endl;
	std::wcout << (s4 == s4) << std::endl;
	return 0;
}