#pragma once
#include <iostream>


namespace Utils
{
	template<class T>
	class MyString
	{
	public:
		//defualt ctor
		MyString();

		//ctor
		//input - otherString null terminated char * to intilaize object
		MyString(const T* otherString);

		//ctor
		//input - otherString MyString to intilaize object
		MyString(MyString<T>& otherString);

		//dtor
		//release memory allocated
		~MyString();

		//get string len (without the null terminator)
		size_t GetLength() const;

		//get string (char*) ptr
		T* getStr() const;

		//Assign My string
		//this = otherString
		//input otherString - object to assign 
		void Assign(const MyString<T>& otherString);

		//Assign My string
		//this = otherString
		//input otherString - char* to assign 
		void Assign(const T* otherString);

		//get char in index location
		//return arr[index]
		//input index - index in char array 
		T CharAt(const size_t index) const;

		//append 2 object
		//this.str += otherString
		//input otherString - object to  append
		void Append(const MyString<T>& otherString);

		//append 2 string
		//this.str += otherString
		//input otherString - object to  append
		void Append(const T* otherString);

		//cmp 2 string
		//strcmp(this, otherString)
		//input otherString - object to compere
		int Compare(const MyString<T>& otherString) const;

		//cmp 2 string
		//strcmp(this, otherString)
		//input otherString - object to compere
		int Compare(const T* otherString) const;

		//check is str length == 0 
		bool IsEmpty() const;

		//release resources
		void Clear();

		bool operator==(const MyString<T>& a);

		MyString& operator=(const MyString<T>& a);

		friend std::ostream &operator<<(std::ostream &os, Utils::MyString<T> const &m)
		{
			os << m.m_str;
			return os;
		}

		friend std::wostream &operator<<(std::wostream &os, Utils::MyString<T> const &m)
		{
			os << m.m_str;
			return os;
		}

	private:
		T* m_str;
		size_t m_length; //strlen
		size_t m_size;   //mem alloc size 
	};

	template<class T>
	class Strcmp
	{
	public:
		int operator()(const T* a, const T* b)
		{
			T *s1 = const_cast<T*>(a);
			T *s2 = const_cast<T*>(b);
			unsigned c1, c2;

			do
			{
				c1 = (T)*s1++;
				c2 = (T)*s2++;
				if(c1 == (T)'\0')
					return c1 - c2;
			} while(c1 == c2);

			return c1 - c2;
		}
	};

	template<class T>
	class Strncat_s
	{
	public:
		T* operator()(T* a, const size_t size_a, const T* b, const size_t size_b)
		{
			T *rsrc = a;
			T *rdest = const_cast<T*>(b);
			while(*rsrc)
				rsrc++;
			while(*rsrc++ = *rdest++)
				;
			return rsrc;
		}
	};

	template<class T>
	class Strlen
	{
	public:
		size_t operator()(const T* a)
		{
			T *rsrc = const_cast<T*>(a);
			while(*rsrc)
				rsrc++;
			return rsrc - a;
		}
	};
}